#!/usr/bin/env python3

import pygame

from random import randint
from time import sleep
from pygame.locals import *

SIZE_SQUARE = 12
SIZE_GRID = 80
SIZE = SIZE_SQUARE * SIZE_GRID

MAX_FOOD = 2

NORTH = 0
EAST  = 1
SOUTH = 2
WEST  = 3


class Vector2D:
    def __init__(self, x, y):
        self.x = x;
        self.y = y;


    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    
    def is_valid(self):
        if self.x < 0 or self.y < 0:
            return False
        if self.x > SIZE_GRID - 1 or self.y > SIZE_GRID - 1:
            return False
        return True


class Food(pygame.sprite.Sprite):
    def __init__(self, surf_size=SIZE_SQUARE):
        super(Food, self).__init__()
        self.surf = pygame.Surface((surf_size, surf_size))
        self.surf_size = surf_size
        self.pos  = Vector2D(randint(0, SIZE_GRID - 1), randint(0, SIZE_GRID - 1))
        self.surf.fill((200, 50, 50))


    def draw(self, scr):
        scr.blit(self.surf, (self.pos.x * self.surf_size, self.pos.y * self.surf_size))


class Snek(pygame.sprite.Sprite):
    def __init__(self, pos, surf_size=SIZE_SQUARE):
        super(Snek, self).__init__()
        self.surf = pygame.Surface((surf_size, surf_size))
        self.surf_size = surf_size
        self.pos = pos
        self.next = None
        self.head = False
        self.surf.fill((100, 200, 100))


    def set_is_head(self, b):
        self.head = b
        if self.head:
            self.surf.fill((100, 200, 100))
        else:
            self.surf.fill((100, 150, 100))


    def draw(self, scr):
        scr.blit(self.surf, (self.pos.x * self.surf_size, self.pos.y * self.surf_size))


    def is_last(self):
        return self.next == None


    def is_second_last(self):
        return self.next.is_last()


class App:
    def __init__(self):
        self._running = True
        self._display = None
        self.size = self.width, self.height = SIZE_SQUARE * SIZE_GRID, SIZE_SQUARE * SIZE_GRID


    def on_init(self):
        # pygame
        pygame.init()
        pygame.display.set_caption("snek")

        self._display = pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF)
        self._running = True
        self.font = pygame.font.SysFont("dejavusansmono", 20)

        # background surface
        self.background = pygame.Surface((SIZE_SQUARE * SIZE_GRID, SIZE_SQUARE * SIZE_GRID))
        self.background.fill((50, 50, 50))

        # game
        self.highscore = 1
        self.max_food = MAX_FOOD
        self.min_speed = 0.06
        self.max_speed = 0.045
        self.cur_speed = self.min_speed

        self.next_walls_are_portals = True
        self.walls_are_portals = True

        self.new_game()


    def new_game(self):
        self.snek = Snek(Vector2D(SIZE_GRID // 2, SIZE_GRID // 2))
        self.snek.next = Snek(Vector2D(SIZE_GRID // 2, SIZE_GRID // 2 + 1))
        self.snek.set_is_head(True)

        self.paused = False
        self.score = 1
        self.overflow = 0

        self.cur_speed = self.min_speed
        self.direction = NORTH
        self.next_direction = NORTH
        self.walls_are_portals = self.next_walls_are_portals

        self.food = []
        for n in range(self.max_food):
            self.food.append(Food())

        self.render_text()


    def render_text(self):
        self.text_score = self.font.render('Score:   ' + str(self.score), True, (100, 200, 100))
        self.text_highscore = self.font.render('High:    ' + str(self.highscore), True, (100, 200, 100))
        self.text_speed = self.font.render('Speed:   ' + str(round(self.min_speed / self.cur_speed, 2)), True, (100, 200, 100))
        if self.next_walls_are_portals == self.walls_are_portals:
            self.text_portals = self.font.render('Portals: ' + str(self.walls_are_portals), True, (100, 200, 100))
        else:
            self.text_portals = self.font.render('Portals: ' + str(self.walls_are_portals) + ' (changed)', True, (100, 200, 100))
        self.text_pause = self.font.render('PAUSED', True, (100, 200, 100))



    def on_event(self, event):
        if event.type == pygame.QUIT:
            self._running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == K_ESCAPE or event.key == K_q:
                self._running = False

            # movement
            elif event.key == K_w and self.direction != SOUTH:
                self.next_direction = NORTH
            elif event.key == K_d and self.direction != WEST:
                self.next_direction = EAST
            elif event.key == K_s and self.direction != NORTH:
                self.next_direction = SOUTH
            elif event.key == K_a and self.direction != EAST:
                self.next_direction = WEST

            # portals
            elif event.key == K_p:
                self.next_walls_are_portals = not self.next_walls_are_portals
                self.render_text()

            # restart
            elif event.key == K_r:
                self.new_game()
            # pause
            elif event.key == K_SPACE:
                self.paused = not self.paused
            # food
            elif event.key == K_PLUS:
                self.max_food += 1
            elif event.key == K_MINUS:
                self.max_food -= 1



    def on_loop(self):
        self.direction = self.next_direction

        # head
        tmp = self.snek
        tmp.set_is_head(False)
        if self.direction == NORTH:
            self.snek = Snek(Vector2D(tmp.pos.x, tmp.pos.y - 1))
        elif self.direction == SOUTH:
            self.snek = Snek(Vector2D(tmp.pos.x, tmp.pos.y + 1))

        elif self.direction == EAST:
            self.snek = Snek(Vector2D(tmp.pos.x + 1, tmp.pos.y))
        elif self.direction == WEST:
            self.snek = Snek(Vector2D(tmp.pos.x - 1, tmp.pos.y))
        self.snek.next = tmp
        self.snek.set_is_head(True)

        # food
        for f in self.food:
            if f.pos == self.snek.pos:
                self.food.remove(f)
                self.overflow += 1
                self.score += 1
                if self.cur_speed > self.max_speed:
                    self.cur_speed -= 0.0005
                if self.score > self.highscore:
                    self.highscore = self.score
                self.render_text()
                break

        while len(self.food) < self.max_food:
            self.food.append(Food())

        # collision (wall)
        if self.walls_are_portals:
            if self.snek.pos.y < 0 and self.direction == NORTH:
                self.snek.pos.y = SIZE_GRID - 1
            elif self.snek.pos.y >= SIZE_GRID and self.direction == SOUTH:
                self.snek.pos.y = 0

            elif self.snek.pos.x < 0 and self.direction == WEST:
                self.snek.pos.x = SIZE_GRID - 1
            elif self.snek.pos.x >= SIZE_GRID and self.direction == EAST:
                self.snek.pos.x = 0

        else:
            if not self.snek.pos.is_valid():
                self.new_game()
                return
        # collision (self)
        n = self.snek.next
        while n:
            if self.snek.pos == n.pos:
                self.new_game()
                return
            n = n.next

        # body
        if self.overflow == 0:
            n = self.snek
            while not n.is_second_last():
                n = n.next
            n.next = None
        else:
            self.overflow -= 1


    def on_render(self):
        # background
        self._display.blit(self.background, (0, 0))

        # food
        for f in self.food:
            f.draw(self._display)

        # snek
        n = self.snek
        while n:
            n.draw(self._display)
            n = n.next

        # text
        self._display.blit(self.text_score, (5, 0))
        self._display.blit(self.text_highscore, (5, 22))
        self._display.blit(self.text_speed, (5, 44))
        self._display.blit(self.text_portals, (5, 66))
        if self.paused:
            self._display.blit(self.text_pause, (5, 88))

        pygame.display.flip()


    def on_cleanup(self):
        pygame.quit()


    def on_execute(self):
        if self.on_init() == False:
            self._running = False
 
        while self._running:
            for event in pygame.event.get():
                self.on_event(event)
            if not self.paused:
                self.on_loop()
            self.on_render()
            sleep(self.cur_speed)
        self.on_cleanup()



if __name__ == "__main__" :
    app = App()
    app.on_execute()


